package ru.dubrovin.module00.task3;

/*
    Внеси изменения в программу, чтобы переменная name имела значение Dima.
*/
public class Task3
{
    public static void main(String[] args)
    {
        String name = "Dima";
        String text = "Hello " + name + "!";
        System.out.println(text);
    }
}