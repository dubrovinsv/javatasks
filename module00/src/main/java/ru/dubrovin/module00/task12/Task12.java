package ru.dubrovin.module00.task12;

/*
Выводим квадрат числа
*/

public class Task12 {
    public static int number = 25;

    public static void main(String[] args) {
        System.out.println(number*number);
    }
}